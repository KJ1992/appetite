﻿var open, website, price_level, name, address, rating, storeLocation, price;


//getting the resturant details, storing them in a global variable
//calling the details function
function restaurantDetails(restRequest, marker, place) {

    restaurants.getDetails(restRequest, function (store, status) {
        storeLocation = place.geometry.location;
 
        name = store.name;
        website = store.website;
        address = store.formatted_address;
        rating = store.rating;

        if (rating == null)
        {
            rating = "0";
        }
        if (website == null) {
            website = "#map";
        }

        try {        
            open = store.opening_hours.periods[1].open.time;
        }
        catch (e) {
            open = 'No Available Time';
        }

        switch (price = store.price_level) {
           /* case 0:
                price = "Free";
                break
            case 1:
                price = "$";
                break
            case 2:
                price = "$$";
                break
            case 3:
                price = "$$$";
                break
            case 4:
                price = "$$$$";
                break*/
            default:
                price = "";
        }



        //infowindow.setContent(store.name + '<br>' + store.formatted_address + '<br>' + 'Rating: ' + store.rating + '<br>' /*+ openClose + '<br>'*/ + '<a href="' + website + '" target="_blank" ">Website</a>' + '<br>' + open + '<br>' + price_level);
        //infowindow.open(map, marker);

        //$(".mrPanel").append(store.name + '<br>' + store.formatted_address + '<br>' + 'Rating: ' + store.rating + '<br>' /*+ openClose + '<br>'*/ + '<a href="' + website + '" target="_blank" ">Website</a>' + '<br>' + open + '<br>' + price_level + "<a href='calculateAndDisplayRoute(directionsService, directionsDisplay, place.geometry.location)'>Directions</a>");
        show();
       // alert(transportationMethod);
        details();
    });
}
//binding the DirectionDisplay object to the map, binding it to the panel
//calling the calculateanddisplayroute function
//making the panel visible and apending a button after the directions which will call the details function   
function direction() {

    $(".mrPanel").empty();

    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById('directions'));
    calculateAndDisplayRoute(directionsService, directionsDisplay, storeLocation);

    $("#directions").show();
    $("#directions").after(" <br/> <input type='button' id='Details' class='pannelButton' onclick='details()' value='Details'>");

}
//removing the details button and hiding the directions
//apending the details in the right panel and a button for the directions
function details() {

    $("#Details").remove();

    $('#directions').hide();

    $(".mrPanel").append(name + '<br>' + address + '<br>' + 'Rating:' + rating + '<br>' + '<a href="' + website + '" target="_blank" ">Website</a>' + '<br>' + open + '<br>' + price + '<br>' + "<input type='button' class='pannelButton' id='direction' onclick='direction()' value='Directions'></input> <input type='button' class='pannelButton' onclick='hide()' value='Hide'></input>");
    
}