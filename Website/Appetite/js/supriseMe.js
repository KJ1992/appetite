﻿function surpriseMe() {
    /* Specify location, radius and place types for your Places API search. */
    var request = {
        location: userLocation,
        radius: mapRadius,
        types: ['restaurant']
    };

    // Create the PlaceService and send the request.
    // Handle the callback with an anonymous function.
    //creating marker for the random restaurant selected
    //calling the restaurant details function
    var restaurants = new google.maps.places.PlacesService(map);
    restaurants.nearbySearch(request, function (results, status) {

        var x = Math.floor((Math.random() * results.length));

        marker.setMap(null);
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            var place = results[x];
            map.setZoom(20);
            map.panTo(place.geometry.location);
            marker.setPosition(place.geometry.location);
            marker.setMap(map);
            marker.setIcon('http://www.maps.google.com/mapfiles/ms/icons/orange-dot.png');
            $(".mrPanel").empty();
            restRequest = {
                reference: place.reference
            };
            restaurantDetails(restRequest, marker, place);
        }
        infowindow.close();
        hide();
    });
}