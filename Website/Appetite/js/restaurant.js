﻿/*
    here we are initializing the map and doing the nearby restaurant search which will do a callback function that will go
    and create markers and add pagination token
*/
function restaurant(loc) {
    try {
        //we are going to empty the panel
        $(".mPanel").empty();
    } catch (e) {

    }
    //initialzing the map
    map = new google.maps.Map(document.getElementById('map'), {
        center: loc,
        zoom: 17
    });
    infowindow = new google.maps.InfoWindow();

    //nearby search
    restaurants = new google.maps.places.PlacesService(map);
    restaurants.nearbySearch({
        location: loc,
        radius: mapRadius,
        type: ['restaurant'],
        keyword: cuisinesTypes
    }, callback);
    function callback(results, status, pagination) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            var restLoc, restName;
            for (var i = 0; i < results.length; i++) {
                createMarker(results[i]);
            }
            if (pagination.hasNextPage) {
                pagination.nextPage();
                }
            }
    }
    //here we are calling the hide function in the animations javascript
    hide();
}
