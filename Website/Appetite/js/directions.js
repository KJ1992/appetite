﻿//-----------------------------------Directions-------------------------------------------------
function calculateAndDisplayRoute(directionsService, directionsDisplay, store) {
    var start = userLocation;    // the variable for the starting/origin point of the directions which is the current campus
    var end = store;    // the variable for the destination in the directions which is the restaurant location
    directionsService.route({
        origin: start,
        destination: end,
        travelMode: transportationMethod
    }, function (response, status) {
        if (status === 'OK') {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}