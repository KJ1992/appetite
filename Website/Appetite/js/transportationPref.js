﻿//Setting the user preferences such as the radius for the nearby search and the cuisine preference
function userPreference() {
    var userRadius;
    var userCuisines;
    // Get the value of the input field with id="numb"
    userRadius = document.getElementById("radius").value;
    userCuisines = document.getElementById("cuisines").value;

    cuisinesTypes = userCuisines;
    mapRadius = userRadius;
    restaurant(userLocation);
    marker.setMap(null);
    hide();
}
//setting the ransportation preferences based off the value we get from the dropdown list
function transportationPref()
{
    var transportationPref;

    transportationPref = document.getElementById("transportationPref").value;

    transportationMethod = transportationPref;
    marker.setMap(null);
    hide();
}