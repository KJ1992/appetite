﻿//hiding the right panel, clearing the direction display on the right panel
function hide() {

    $("#map").width("80%");
    $(" #right-panel ").hide();
    directionsDisplay.setMap(null);
    directionsDisplay.setPanel(null);
    infowindow.close();
}
//making the panel visible
function show() {
    $("#map").animate({
        width: "60%"
    }, {
        duration: 50,
        specialEasing: {
            width: 'linear'
        }
    });
    $(" #right-panel ").show(50);
}