﻿//here we are creating the left panel which will contain all the restaurant names
//creating a marker for each location
//markers for all 3 campus locations
function createMarker(place) {
    var bMarker = new google.maps.Marker({
        position: bCampus,
        map: map,
        title: 'Sheridan Davis Campus',
        icon: sheridan
    });
    var mMarker = new google.maps.Marker({
        position: mCampus,
        map: map,
        title: 'Sheridan Hazel McCalllon Campus',
        icon: sheridan
    });
    var tMarker = new google.maps.Marker({
        position: tCampus,
        map: map,
        title: 'Sheridan Trafalgar Campus',
        icon: sheridan
    });

    var placeLoc = place.geometry.location;

    marker = new google.maps.Marker();
  
    //for each location we are appending the right panel with the locations name with a click function and marker for the 
    //location
    $(placeLoc).each(function () {
        $("<li style='cursor:pointer' />")
            .html(place.name)
            .click(function () {
                map.setZoom(17);
                map.setCenter(place.geometry.location);
                
                $(".mrPanel").empty();
                marker.setMap(null);
                hide();
                marker = new google.maps.Marker({
                    map: map,
                    position: place.geometry.location,
                    icon: 'http://www.maps.google.com/mapfiles/ms/icons/orange-dot.png'
                });
                restRequest = {
                    reference: place.reference
                };
                
                google.maps.event.addListener(marker, 'click', function () {
                    $(".mrPanel").empty();
                    restaurantDetails(restRequest, marker, place);
                });
                restaurantDetails(restRequest, marker, place);
                return marker;
            })
            .appendTo(".mPanel");
    });
}