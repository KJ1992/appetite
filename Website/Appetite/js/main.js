﻿var infoWindow; //global variable for the infowindow
var map;    //global variable for the map
var marker; //global variable for the marker
var restRequest;

//campus locations
var bCampus = { lat: 43.656054, lng: -79.739344 };
var mCampus = { lat: 43.591351, lng: -79.647188 };
var tCampus = { lat: 43.469015, lng: -79.7008127 };
//-------------------------------------------------
var sheridan = 'img/SheridanMarker.png';
var userLocation = bCampus;
var restaurants;     //global variable for restaurants
var mapRadius = 500;        //current default map radius
var cuisinesTypes = '';     //default cuisines type
var transportationMethod = "WALKING";       //default transportation method

//variables to hold DirectionRenderer and DirectionsService objects
var directionsDisplay;
var directionsService;
