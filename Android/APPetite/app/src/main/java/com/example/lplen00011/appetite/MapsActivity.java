package com.example.lplen00011.appetite;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
/**
       Displays the selected restaurant to the user along with a map of how close the restaurant is to the school
       Allows the user to call, favourite, and visit the website of the restaurant.
 */

public class MapsActivity extends FrameActivity implements
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback {

    private GoogleMap mMap;
    private final static LatLng sheridanD = new LatLng(43.65632, -79.74002);
    private final static LatLng sheridanT = new LatLng(43.470169, -79.701948);
    private final static LatLng sheridanH = new LatLng(43.591096, -79.6477065);
    LatLng newMarker;
    String name, placeID, address, open, jsonString, number, website;
    double markerlat, markerlng;
    DatabaseHelper myDB;
    TextView textName, textAddress, textPhone;
    ImageButton favourite;
    JSONObject jsonObject, grab;
    Typeface nicknormal,dense;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_maps, null);
        drawerLayout.addView(contentView, 0);
        myDB = new DatabaseHelper(this);
        nicknormal = Typeface.createFromAsset(getAssets(),"fonts/nickainleynormal.ttf");
        dense = Typeface.createFromAsset(getAssets(),"fonts/denseregular.ttf");
        try {
            textName = (TextView) findViewById(R.id.mName);
            textPhone = (TextView) findViewById(R.id.phoneNum);
            textAddress = (TextView) findViewById(R.id.mAddress);
            name = getIntent().getExtras().getString("mName");
            favourite = (ImageButton) findViewById(R.id.favorBtn);
            placeID = getIntent().getExtras().getString("sendID");
            Log.d("placeID", placeID + "");
            address = getIntent().getExtras().getString("mAddress");
            open = getIntent().getExtras().getString("isOpen");
            markerlat = Double.parseDouble(getIntent().getExtras().getString("sendLat"));
            markerlng = Double.parseDouble(getIntent().getExtras().getString("sendLng"));
            textName.setText(name);
            textAddress.setText(address);
            textName.setTypeface(nicknormal);
            textAddress.setTypeface(dense);

        } catch (Exception e) {
            Toast.makeText(getBaseContext(), "Pick a place first", Toast.LENGTH_SHORT).show();
        }
        newMarker = new LatLng(markerlat, markerlng);

        if (googleServicesAvailable()) {
            //setContentView(R.layout.activity_maps);
            initMap();
        }
        new GrabPhone().execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    //checks if the phone supports google play services
    public boolean googleServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else {
            Toast.makeText(this, "Cannot connect to google play services", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.addMarker(new MarkerOptions().position(newMarker).title(name)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));

        if (address.contains("Brampton")) {
            mMap.addMarker(new MarkerOptions().position(sheridanD).title("Sheridan Davis")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.shermarker)));
        } else if (address.contains("Oakville")) {
            mMap.addMarker(new MarkerOptions().position(sheridanT).title("Sheridan Trafalgar")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.shermarker)));
        } else if (address.contains("Mississauga")) {
            mMap.addMarker(new MarkerOptions().position(sheridanH).title("Sheridan HMC")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.shermarker)));
        }


        mMap.moveCamera(CameraUpdateFactory.newLatLng(newMarker));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(13));
        //mMap.setLatLngBoundsForCameraTarget(newMarker);
        mMap.setOnMarkerClickListener(this);
    }


    @Override
    public boolean onMarkerClick(final Marker marker) {

        return false;
    }

    @Override
    public void onBackPressed() {
        Intent h = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(h);
        finish();
    }

    //Adds List Items to Favourites
    public void addToFavourites(View view) {
        if (favourite.getTag().equals("noFill")) {
            boolean added = myDB.addData(placeID, name, address);
            if (added) {
                Toast.makeText(this, "Added to favourites ", Toast.LENGTH_SHORT).show();
                favourite.setBackgroundResource(R.drawable.heartfilled50);
                favourite.setTag("filled");
                delayClick();
            } else
                Toast.makeText(this, "Error, Couldn't Add ", Toast.LENGTH_SHORT).show();
        } else if (favourite.getTag().equals("filled")) {

        }
    }

    // Used to stop the button from being rapidly clicked
    public void delayClick() {
        favourite.setEnabled(false);
        Timer buttonTimer = new Timer();
        buttonTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        favourite.setEnabled(true);
                    }
                });
            }
        }, 3);
    }

    // Gets number based of place_ID
    private void numberPlease() {
        try {
            if (jsonString != null) {
                jsonObject = new JSONObject(jsonString);
                grab = jsonObject.getJSONObject("result");
                if (grab.has("formatted_phone_number")) {
                    number = grab.getString("formatted_phone_number");
                } else {
                    number = "Number Not Available";
                }
                if (grab.has("website")) {
                    website = grab.getString("website");
                } else {
                    website = "None";
                }
                Log.d("Number", number + "");
                Log.d("Website", website + "");
                textPhone.setText(number);
                textPhone.setTypeface(dense);
            } else {
                Toast.makeText(this, "Json String Didn't Worked", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void callPlace(View view) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:"+ number));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Call Permissions Not Granted, Call Cancelled ", Toast.LENGTH_LONG).show();
        }
        else {
            if(number != "Number Not Available")
            startActivity(callIntent);
            else
                Toast.makeText(this, "No Number Was Available To Call ", Toast.LENGTH_SHORT).show();
        }

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        else
        {
            return false;
        }
    }



    public void gotoWebsite(View view) {
        Intent webIntent = new Intent(Intent.ACTION_VIEW);
        webIntent.setData(Uri.parse(website));
        if(isNetworkAvailable()== true)
        {
            if(website !="None")
            {
                startActivity(webIntent);
            }
            else
            {
                Toast.makeText(this, "No Website Available", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }


    }

    // Grabs and formats
    private class GrabPhone extends AsyncTask<Void, Void, String> {
        String myQuery;
        String JsonValues;
        private final String apiKey = "AIzaSyAzd6Cbr1A242OdtxJIXFR5BjA9VDp5wA8";

        @Override
        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL(myQuery);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                InputStream iStream = connection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(iStream));
                StringBuilder builder = new StringBuilder();
                while ((JsonValues = bufferedReader.readLine()) != null) {
                    builder.append(JsonValues + "\n");
                }
                bufferedReader.close();
                iStream.close();
                connection.disconnect();
                return builder.toString().trim();
            } catch (MalformedURLException e) // URL connection
            {
                e.printStackTrace();
            } catch (IOException e) // BufferedReader, InputStream
            {
                e.printStackTrace();
                return "Error";
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            myQuery = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeID + "&key="+apiKey;
            //https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJtZFloZcVK4gRIifF1NaxUis&key=AIzaSyAzd6Cbr1A242OdtxJIXFR5BjA9VDp5wA8
        }


        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("Error")) {
                Toast.makeText(getBaseContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
            } else {
                jsonString = result;
                numberPlease();
            }
        }

    }
}

