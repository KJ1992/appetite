package com.example.lplen00011.appetite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 The
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DatabaseName = "Appetite.db";
    public static final String TableName = "favouritesTable";
    public static final String Col1 = "ID";
    public static final String Col2 = "PlaceID";
    public static final String Col3 = "Name";
    public static final String Col4 = "Address";

    public DatabaseHelper(Context context) {
        super(context, DatabaseName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqldb) {
       sqldb.execSQL("CREATE TABLE " + TableName +" (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "PLACEID TEXT, NAME TEXT, ADDRESS TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqldb, int i, int i1) {
        sqldb.execSQL("DROP TABLE IF EXISTS " + TableName);
        onCreate(sqldb);
    }

    public boolean addData(String placeID, String name, String address)
    {
        SQLiteDatabase sqldb = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Col2,placeID);
        values.put(Col3,name);
        values.put(Col4,address);
        long result = sqldb.insert(TableName,null,values);
        if (result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public Cursor getAllData() throws SQLException
    {
        SQLiteDatabase sqldb = this.getWritableDatabase();
        Cursor pointer = sqldb.rawQuery("SELECT * FROM " + TableName,null);
        return pointer;
    }

    public synchronized void close() {
        SQLiteDatabase sqldb = this.getWritableDatabase();
        if (sqldb != null)
            sqldb.close();
        super.close();
    }

    public Integer deleteData(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TableName, "ID = ?",new String[] {id});
    }
}
