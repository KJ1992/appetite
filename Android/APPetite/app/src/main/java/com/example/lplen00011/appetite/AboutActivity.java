package com.example.lplen00011.appetite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
/**
    Just a display page for the JLPD Enterprise group and
    any icons or pictures we used that needs to be credited.
 */

public class AboutActivity extends FrameActivity {
 // icons provided by icons 8

    TextView aboutUs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_about, null);
        drawerLayout.addView(contentView, 0);
        aboutUs = (TextView) findViewById(R.id.aboutUs);
        aboutUs.setText("The was a capstone project developed by \n" +
        "Kurtis Johnson, Mharc Paccanas, Solomon Laxmidhar, and Dhondup Dorjee \n" +
        "All menu Icons were graciously provided by https://icons8.com/");

    }
}