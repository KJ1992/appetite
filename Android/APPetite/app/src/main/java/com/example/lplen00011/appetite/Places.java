package com.example.lplen00011.appetite;



/**
  Used to group together data of a specific restaurant.
  mainly a getter and setter
 */
public class Places {
    private String id;
    private String name;
    private String hours;
    private double rating;
    private String vicinity;
    private double latitude;
    private double longitude;

    public Places (String id, String name, String hours, double rating, String vicinity, double latitude, double longitude)
    {
        this.setId(id);
        this.setName(name);
        this.setHours(hours);
        this.setRating(rating);
        this.setVicinity(vicinity);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getHours() {
        return hours;
    }
    public void setHours(String hours) {
        this.hours= hours;
    }
    public double getRating() {
        return rating;
    }
    public void setRating(double rating) {
        this.rating = rating;
    }
    public String getVicinity() {
        return vicinity;
    }
    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

}

