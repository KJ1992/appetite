package com.example.lplen00011.appetite;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
/**
    Allows the user to select a campus, the selected campus is saved to shared preferences.
    The data saved includes campus name, longitude, and latitude.
 */


public class SheridanActivity extends FrameActivity {

    ImageButton displayDavis;
    ImageButton displayTrafalgar;
    ImageButton displayHMC;
    private double longitude;
    private double latitude;
    private String name;
    private TextView txtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_sheridan, null);
        drawerLayout.addView(contentView, 0);
        displayDavis = (ImageButton) findViewById(R.id.iBtnDavis);
        displayTrafalgar = (ImageButton) findViewById(R.id.iBtnTrafalgar);
        displayHMC = (ImageButton) findViewById(R.id.iBtnHMC);
        txtTitle = (TextView) findViewById(R.id.mySherTitle);
        Typeface nicknormal= Typeface.createFromAsset(getAssets(),"fonts/nickainleynormal.ttf");
        txtTitle.setTypeface(nicknormal);
        displayDavis.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                name = "Davis Campus";
                longitude = -79.74002;
                latitude = 43.65632;
                sendBackLatLong(name, latitude,longitude);

            }
        });

        displayTrafalgar.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                name = "Trafalgar Campus";
                longitude = -79.701948;
                latitude = 43.470169;
                sendBackLatLong(name, latitude,longitude);

            }
        });

        displayHMC.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                name = "HMC Campus";
                longitude = -79.6477065;
                latitude = 43.591096;
                sendBackLatLong(name, latitude,longitude);
            }
        });

    }

    private void sendBackLatLong(String name, double latitude, double longitude)
    {
        SharedPreferences userPrefs = getSharedPreferences("userPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userPrefs.edit();
        editor.putString("name", name);
        editor.putString("latitude", String.valueOf(latitude));
        editor.putString("longitude", String.valueOf(longitude));
        editor.commit();

        Intent h = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(h);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent h = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(h);
        finish();
    }

}