package com.example.lplen00011.appetite;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
    Beginning Screen for the android app,
    adds a professional look to application.
 */
public class SplashScreen extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        Thread myThread = new Thread() {
            @Override
            public void run()
            {
                try {
                    sleep(1750);
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();
    }
}
