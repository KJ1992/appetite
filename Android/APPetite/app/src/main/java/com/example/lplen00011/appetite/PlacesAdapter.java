package com.example.lplen00011.appetite;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;


/**
  Takes the data from the Places class and
 inserts the data inside their respective textviews to make up one listview.
 Distace is calcuated here using an android distance method along with lat and long coordinates
 retrieved by sharedpreferences and intent data.
 campus lat & long coordinates is from shared preferences while the restaurant coordinates
 is retrived by the intent passing the data from one class to another.
 */

public class PlacesAdapter extends ArrayAdapter {
    List restuarants = new ArrayList();
    private double mylong, mylat;
    Location thisSheridan;
    Location thisRestaurant;
    Typeface nicknormal, dense;


    public PlacesAdapter(Context context, int resources) {
        super(context,resources);
        nicknormal = Typeface.createFromAsset(context.getAssets(),"fonts/nickainleynormal.ttf");
        dense = Typeface.createFromAsset(context.getAssets(),"fonts/denseregular.ttf");
    }

    public void add(Object object)
    {
        super.add(object);
        restuarants.add(object);
    }

    public int getCount()
    {
        return restuarants.size();
    }

    public Object getItem(int position)
    {
        return restuarants.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row;
        row = convertView;
        PlacesContainer placesContainer;
        getSheridanLocation();
        if(row == null)
        {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.custom_layout,parent,false);
            placesContainer = new PlacesContainer();
            placesContainer.txname = (TextView) row.findViewById(R.id.txName);
            placesContainer.txaddress = (TextView) row.findViewById(R.id.txAddress);
            placesContainer.txhours = (TextView) row.findViewById(R.id.txHours);
            placesContainer.txDistance = (TextView) row.findViewById(R.id.txDistance);
            placesContainer.rtBar = (RatingBar) row.findViewById(R.id.ratingBar);
            row.setTag(placesContainer);

        }
        else
        {
            placesContainer = (PlacesContainer) row.getTag();
        }
        Places places = (Places) this.getItem(position);
        placesContainer.txname.setText(places.getName());
        placesContainer.txname.setTag(places.getId());
        placesContainer.txaddress.setText(places.getVicinity());
        placesContainer.txaddress.setTag(R.id.tag1Id,places.getLatitude());
        placesContainer.txaddress.setTag(R.id.tag2Id,places.getLongitude());
        placesContainer.txhours.setText(places.getHours());
        placesContainer.rtBar.setRating((float)(places.getRating()));
        placesContainer.txDistance.setText(setDistance(places.getLongitude(), places.getLatitude()));

        placesContainer.txname.setTypeface(nicknormal);
        placesContainer.txaddress.setTypeface(dense);
        placesContainer.txhours.setTypeface(dense);
        placesContainer.txDistance.setTypeface(dense);
        return row;
    }

    private String setDistance(double lng, double lat)
    {
        thisRestaurant = new Location("");
        thisRestaurant.setLatitude(lat);
        thisRestaurant.setLongitude(lng);
        float sendDistance = thisSheridan.distanceTo(thisRestaurant);
        if(sendDistance > 1000) {
            return String.format("%.2f",sendDistance/1000) + " Km";
        } else {
            return String.format("%.0f",sendDistance) + " m";
        }


       /* Haversine Formula - A bit too slow however  more accurate
        double haversineA,haversineC,haversineD,totalLong,totalLat;
        double earthRadius = 6371
        totalLat = Math.toRadians(lat - mylat);
        totalLong = Math.toRadians(lng - mylong);
        haversineA = Math.pow(Math.sin(totalLat/2),2) + Math.cos(Math.toRadians(mylat)) * Math.cos(Math.toRadians(lat)) * Math.pow(Math.sin(totalLong/2),2);
        haversineC = 2 * Math.atan2(Math.sqrt(haversineA), Math.sqrt(1 - haversineA));
        haversineD = Math.round((earthRadius * haversineC)/1000);
        return String.valueOf(haversineD) + " Km";*/
    }

    public void getSheridanLocation() {
        Context applicationContext = MainActivity.getContextOfApplication();
        SharedPreferences sp = applicationContext.getSharedPreferences("userPrefs", Context.MODE_PRIVATE);
        String readLat = sp.getString("latitude", "No Lat");
        String readLong = sp.getString("longitude", "No Long");
        if (readLat == "No Lat" || readLong == "No Long")
        {
            mylat = 43.65632;
            mylong = -79.74002;

        }
        else
        {
            mylat = Double.parseDouble(readLat);
            mylong = Double.parseDouble(readLong);
        }
        thisSheridan = new Location("");
        thisSheridan.setLatitude(mylat);
        thisSheridan.setLongitude(mylong);
    }


    static class PlacesContainer
    {
        RatingBar rtBar;
        TextView txname, txaddress, txhours, txDistance;
    }

}


