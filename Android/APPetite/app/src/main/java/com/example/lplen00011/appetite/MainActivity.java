package com.example.lplen00011.appetite;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
    This class is more of a menu to direct users to either the restuarants/sheridan/preferences pages
    If the user clicks restaurants right away there are default values to prevent the app from crashing
    Otherwise any changed will be retrieved from their respective shared preferences file.

    This class is also responsible for parsing the json string for the
    google places API and sending over to the MyList Activity.
 */

public class MainActivity extends FrameActivity {
    private ImageButton sheridanBn, beginBn, settingsBn;
    private String jsonString;
    private double longitude,latitude;
    private float radius;
    private String name;
    private String type;
    private final String apiKey = "AIzaSyAzd6Cbr1A242OdtxJIXFR5BjA9VDp5wA8";
    public static Context myContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_main, null);
        drawerLayout.addView(contentView, 0);
        sheridanBn = (ImageButton) findViewById(R.id.sheridanBtn);
        settingsBn = (ImageButton) findViewById(R.id.settingsBtn);
        beginBn = (ImageButton) findViewById(R.id.beginBtn);
        myContext = getApplicationContext();


        sheridanBn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent s = new Intent(getApplicationContext(), SheridanActivity.class);
                startActivity(s);
                finish();
            }
        });

        settingsBn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent s = new Intent(getApplicationContext(), PreferredActivity.class);
                startActivity(s);
                finish();
            }
        });

        beginBn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                checkValues();
                parseJSON(v);

            }
        });
        checkValues();
        new GrabPlaces().execute();
    }

    public static Context getContextOfApplication() {
        return myContext;
    }


    public void parseJSON(View v) {
        new GrabPlaces().execute();

        if (jsonString != null) {
            Intent i = new Intent(MainActivity.this, MyList.class);
            i.putExtra("json_data", jsonString);
            i.putExtra("name", name);
            startActivity(i);
        }

    }

    public void checkValues() {
        SharedPreferences sp = getSharedPreferences("userPrefs", Context.MODE_PRIVATE);
        String sname = sp.getString("name", "No Name");
        String readLat = sp.getString("latitude", "No Lat");
        String readLong = sp.getString("longitude", "No Long");
        String stype = sp.getString("cusine", "None");
        float rdistance = sp.getFloat("radius", 0);
        if (readLat.equals("No Lat") || readLong.equals("No Long") || sname.equals("No Name")) {
            name = "Davis Campus";
            longitude = -79.74002;
            latitude = 43.65632;
        } else {
            name = sname;
            latitude = Double.parseDouble(readLat);
            longitude = Double.parseDouble(readLong);
        }

        if( stype.equals("None"))
        {
            type = "None";
        }
        else
        {
            type = stype;
        }

        if (rdistance == 0)
        {
            radius = 500;
        }
        else
        {
            radius = rdistance;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class GrabPlaces extends AsyncTask<Void, Void, String> {
        String myQuery;
        String JsonValues;

        @Override
        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL(myQuery);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                InputStream iStream = connection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(iStream));
                StringBuilder builder = new StringBuilder();
                while ((JsonValues = bufferedReader.readLine()) != null) {
                    builder.append(JsonValues + "\n");
                }
                bufferedReader.close();
                iStream.close();
                connection.disconnect();
                return builder.toString().trim();
            } catch (MalformedURLException e) // URL connection
            {
                e.printStackTrace();
            } catch (IOException e) // BufferedReader, InputStream
            {
                e.printStackTrace();
                return "Error";
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            if (type.equals("None")) {
                myQuery = "https://maps.googleapis.com/maps/api/place/search/json?location=" + latitude + "," + longitude + "&radius=" + radius
                        + "&type=restaurant" + "&hasNextPage=true&nextPage()=true&sensor=true&key=" + apiKey + "";
            }
            else
            {
                myQuery = "https://maps.googleapis.com/maps/api/place/search/json?location=" + latitude + "," + longitude + "&radius=" + radius
                        + "&type=restaurant&keyword="+ type + "&hasNextPage=true&nextPage()=true&sensor=true&key=" + apiKey + "";
            }

            //JSON URL
            //"https://maps.googleapis.com/maps/api/place/search/json?location=43.65632,-79.74002&radius=5000&type=restaurant&key=AIzaSyAzd6Cbr1A242OdtxJIXFR5BjA9VDp5wA8";
            //https://maps.googleapis.com/maps/api/place/search/json?location=43.65632,-79.74002&radius=5000&type=restaurant&keyword=chinese&key=AIzaSyAzd6Cbr1A242OdtxJIXFR5BjA9VDp5wA8
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("Error")) {
                Toast.makeText(getBaseContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
            } else {
                jsonString = result;
            }
        }
    }
}