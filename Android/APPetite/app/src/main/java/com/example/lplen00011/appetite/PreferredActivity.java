package com.example.lplen00011.appetite;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

/**
    Allows the user to select a radius/cuisine, and saved data to shared preferences.
    The data saved may include preferred radius and/or cuisine.
    It should be noted that you don't keep selecting both preferences to save the values.
 */

public class PreferredActivity extends FrameActivity {
    private RadioGroup selectDistance;
    private RadioButton prefDistance, rb05,rb25,rb50;
    private ImageButton submit;
    private Spinner foodSpinner;
    private String cusine;
    private float radius;
    private TextView txtRadius, txtType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_preferred, null);
        drawerLayout.addView(contentView, 0);
        foodSpinner = (Spinner) findViewById(R.id.spinnerfood);
        selectDistance = (RadioGroup) findViewById(R.id.schoolRDO);
        submit = (ImageButton) findViewById(R.id.selectPrefBtn);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.cusine, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        foodSpinner.setAdapter(adapter);
        foodSpinner.setPrompt("Select A Cusine");
        txtRadius = (TextView) findViewById(R.id.prefTitle);
        txtType = (TextView) findViewById(R.id.cusineTitle);
        rb05 = (RadioButton) findViewById(R.id.distance05);
        rb25 = (RadioButton) findViewById(R.id.distance2);
        rb50 = (RadioButton) findViewById(R.id.distance5);
        Typeface nicknormal= Typeface.createFromAsset(getAssets(),"fonts/nickainleynormal.ttf");
        Typeface dense = Typeface.createFromAsset(getAssets(),"fonts/denseregular.ttf");
        rb05.setTypeface(dense);
        rb25.setTypeface(dense);
        rb50.setTypeface(dense);
        txtRadius.setTypeface(nicknormal);
        txtType.setTypeface(nicknormal);

        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int rdoSelected = selectDistance.getCheckedRadioButtonId();
                prefDistance = (RadioButton) findViewById(rdoSelected);
                if (prefDistance != null) {
                    String options = prefDistance.getText().toString();

                    switch (options) {
                        case "500 m":
                            radius = 500;
                            break;
                        case "2.5 Km":
                            radius = 2500;
                            break;
                        case "5 Km":
                            radius = 5000;
                            break;
                    }
                }
                sendRadius(radius);
                sendCusine(cusine);
            }
        });

        foodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                ((TextView) parent.getChildAt(0)).setTextSize(30);
                switch (pos) {
                    case 0:
                        cusine = "None";
                        break;
                    case 1:
                        cusine = "Chinese";
                        break;
                    case 2:
                        cusine = "Hamburger";
                        break;
                    case 3:
                        cusine = "Indian";
                        break;
                    case 4:
                        cusine = "FastFood";
                        break;
                    case 5:
                        cusine = "Greek";
                        break;
                    case 6:
                        cusine = "Mediterranean";
                        break;
                    case 7:
                        cusine = "Pizza";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                cusine = "None";
            }
        });
    }

    public void sendRadius(float radius) {
        SharedPreferences userPrefs = getSharedPreferences("userPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userPrefs.edit();
        editor.putFloat("radius", radius);
        editor.commit();

    }

    public void sendCusine(String cusine) {
        SharedPreferences userPrefs = getSharedPreferences("userPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userPrefs.edit();
        editor.putString("cusine", cusine);
        editor.commit();

        Intent h = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(h);
        finish();
    }


    @Override
    public void onBackPressed() {
        Intent h = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(h);
        finish();
    }
}
