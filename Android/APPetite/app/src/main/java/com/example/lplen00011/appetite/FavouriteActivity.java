package com.example.lplen00011.appetite;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 Displays the list of favourite restaurants saved by the user
 The restaurants are retrieved by the by the database they're saved in, send to FavouritePlaces
 Class for formatting and add to another custom listview via the FavouritePlacesAdapter Class
 */


public class FavouriteActivity extends FrameActivity {

    FavouritePlacesAdapter favouritePlacesAdapter;
    ListView listView;
    DatabaseHelper myDB;
    ImageButton remove;
    TextView textID;
    String placeId;
    ArrayList<FavouritePlaces> arrayList = new ArrayList<FavouritePlaces>();
    Cursor pointer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_favourite, null);
        drawerLayout.addView(contentView, 0);
        listView = (ListView) findViewById(R.id.favList);
        favouritePlacesAdapter = new FavouritePlacesAdapter(this,R.layout.custom_layout2,arrayList);
        listView.setAdapter(favouritePlacesAdapter);
        myDB = new DatabaseHelper(this);
        textID = (TextView) findViewById(R.id.txID);
        remove = (ImageButton) findViewById(R.id.removeBtn);
        populateList();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                TextView textName = (TextView) view.findViewById(R.id.txFaveName);
                TextView textAddress = (TextView) view.findViewById(R.id.txFaveAddress);
                textName.setTag(placeId);
            }
        });
    }



// REWORK**********************************************************************
    public void removeFavourites(View v) {
        //showMessage("No", "Nothing Found");
        //arrayList.remove(listView);
      //  favouritePlacesAdapter = new FavouritePlacesAdapter(this,1,arrayList);
       // listView.setAdapter(favouritePlacesAdapter);
        textID = (TextView) findViewById(R.id.txID);
        remove = (ImageButton) findViewById(R.id.removeBtn);
        Integer delete = myDB.deleteData(textID.getText().toString());
        if (delete > 0) {
            showMessage("Success!", "Item Removed from Favourites",true);
        } else {
            showMessage("Error", "Couldn't Delete",false);
        }
    }

    private void populateList() {
        try {
            pointer = myDB.getAllData();
            if (pointer != null)
            {
                if (pointer.moveToFirst())
                {
                    do {
                        FavouritePlaces fp = new FavouritePlaces();
                        fp.setId(pointer.getString(0));
                        fp.setPlaceID(pointer.getString(1));
                        placeId = pointer.getString(1);
                        fp.setName(pointer.getString(2));
                        fp.setAddress(pointer.getString(3));
                        arrayList.add(fp);
                    }
                    while (pointer.moveToNext());
                }
            }
            else
            {
                showMessage("Error", "Nothing Found",false);
            }
        } catch (SQLException e)
        {
            favouritePlacesAdapter.notifyDataSetChanged();
        }

    }

    public void showMessage(String title, String message, final boolean refresh) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
                if (refresh)
                    refresh();
            }});
        builder.setCancelable(false);
        builder.show();
    }


    public void refresh() {
        Intent refresh = new Intent(this, FavouriteActivity.class);
        startActivity(refresh);
        this.finish();
    }
}