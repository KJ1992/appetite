package com.example.lplen00011.appetite;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;


/**
    The is the main layout of the app, allows for the
    navigation drawer to be displayed on all activites.
 */
public class FrameActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private ActionBarDrawerToggle toggle;
    protected DrawerLayout drawerLayout;

    //Maps API Key = AIzaSyB4x7RaTIdAl548MEGUbHxnyiztrT8C4p0
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_frame);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);


        // Enables the listener to listen for clicks on action bar
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        //Displays nav menu
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);


    }

    //If nav bar is click the hidden menu will appear
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(toggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    //Hidden menu Item listener
    // The Flags are used to clear any app activity on the stack
    // Allows the homepage be exited via back
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.nav_menu1:
                Intent h = new Intent(getApplicationContext(), MainActivity.class);
                h.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                h.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                h.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(h);
                finish();
                break;

            case R.id.nav_menu2:
                Intent f = new Intent(getApplicationContext(), FavouriteActivity.class);
                startActivity(f);
                finish();
                break;

            case R.id.nav_menu3:
                Intent a = new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(a);
                finish();
                break;
        }

        //Hides menu once a menu item is selected
        DrawerLayout hide = (DrawerLayout) findViewById(R.id.drawerLayout);
        if(hide.isDrawerOpen(GravityCompat.START))
        {
            hide.closeDrawer(GravityCompat.START);
        }

        return false;
    }
}
