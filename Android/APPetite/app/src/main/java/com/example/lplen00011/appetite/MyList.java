package com.example.lplen00011.appetite;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Random;
/**
        Displays the list of restaurants to the user
        The json string is retrieved from the MainActivity Class where it is then filtered through for certain results
        Once the all the required results for a restaurant is retrieved that data is send to the Places Class for formatting
        and then added to the list through the PlacesAdapter Class.
        The list is displayed through a custom listview layout called custom_layout.xml
        The surprise me button is also located in this class it is basically a random number generator
        that compares the random number to the listview item ID to focus on a match.
 */

public class MyList extends FrameActivity {
    String jsonString,name;
    JSONObject jsonObject;
    JSONArray jsonArray;
    PlacesAdapter placesAdapter;
    ListView listView;
    TextView txtList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_my_list, null);
        drawerLayout.addView(contentView, 0);

        listView = (ListView) findViewById(R.id.MyList);
        placesAdapter = new PlacesAdapter(this,R.layout.custom_layout);
        listView.setAdapter(placesAdapter);
        txtList = (TextView) findViewById(R.id.txtList);
        name = getIntent().getExtras().getString("name");
        Typeface nicknormal= Typeface.createFromAsset(getAssets(),"fonts/nickainleynormal.ttf");
        txtList.setTypeface(nicknormal);
        jsonString = getIntent().getExtras().getString("json_data");
        txtList.setText("Results for " + name);

        try {
            jsonObject = new JSONObject(jsonString);
            jsonArray = jsonObject.getJSONArray("results");
            String id,name,hours,vicinity;
            double rating,destLat, destLong;

            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject grab = jsonArray.getJSONObject(i);
                JSONObject nestedGrab = grab.getJSONObject("geometry");

                id = grab.getString("place_id");
                name = grab.getString("name");
                if (grab.has("rating")) {
                    rating = grab.getDouble("rating");
                }
                else {
                    rating = 0;
                }
                vicinity = grab.getString("vicinity");
                destLat = nestedGrab.getJSONObject("location").getDouble("lat");
                destLong = nestedGrab.getJSONObject("location").getDouble("lng");
                if (grab.has("opening_hours")) {
                    hours = grab.getJSONObject("opening_hours").getString("open_now");
                    if (hours.equals("false"))
                    {
                        hours = "Not Open";
                    }
                    else if (hours.equals("true"))
                    {
                        hours = "Open";
                    }
                }
                else
                {
                    hours ="Not Available";
                }

                Places places = new Places(id,name,hours,rating,vicinity,destLat,destLong);
                placesAdapter.add(places);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                TextView textName = (TextView) view.findViewById(R.id.txName);
                TextView textAddress = (TextView) view.findViewById(R.id.txAddress);

                Intent s = new Intent(getApplicationContext(), MapsActivity.class);
                s.putExtra("mName",textName.getText().toString());
                s.putExtra("sendID",textName.getTag().toString());
                s.putExtra("mAddress",textAddress.getText().toString());
                s.putExtra("sendLat",textAddress.getTag(R.id.tag1Id).toString());
                s.putExtra("sendLng",textAddress.getTag(R.id.tag2Id).toString());
                startActivity(s);
                finish();
            }
        });
    }

    public void randomChoice(View v) {
        Random rand = new Random();
        int rPosition = rand.nextInt(listView.getCount());


        for (int i = 0; i < listView.getCount(); i++) {
            Log.d("int i", i + "");
            if (rPosition == i ) {
                listView.setSelection(i);
                listView.requestFocus();
                listView.setItemChecked(i, true);
                {
                    //listView.getSelectedItemPosition();
                    // v.setBackgroundColor(Color.BLUE);
                }
                listView.setItemChecked(i, false);
                {
                    // listView.getSelectedItemPosition();
                    //v.setBackgroundColor(Color.TRANSPARENT);
                }
            }

        }
    }

}
