package com.example.lplen00011.appetite;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;


/**
 * Same funcationality as PlacesAdapter Class just for favourites
 */
public class FavouritePlacesAdapter extends ArrayAdapter<FavouritePlaces> {
    private Activity activity;
    int id;
    Typeface nicknormal, dense;
    ArrayList<FavouritePlaces> faveRestaurants;


    public FavouritePlacesAdapter(Activity context, int resources, ArrayList<FavouritePlaces> objects) {
        super(context,resources, objects);
        this.activity = context;
        this.id = resources;
        this.faveRestaurants=objects;
        nicknormal = Typeface.createFromAsset(context.getAssets(),"fonts/nickainleynormal.ttf");
        dense = Typeface.createFromAsset(context.getAssets(),"fonts/denseregular.ttf");
    }

    @Override
    public FavouritePlaces getItem(int position) {
        return faveRestaurants.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        if(row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.custom_layout2,parent,false);
        }
        FavouritePlaces favouritePlaces = faveRestaurants.get(position);
        TextView id = (TextView) row.findViewById(R.id.txID);
        TextView name = (TextView) row.findViewById(R.id.txFaveName);
        TextView address = (TextView) row.findViewById(R.id.txFaveAddress);

        id.setText(favouritePlaces.getId());
        name.setText(favouritePlaces.getName());
        name.setTag(favouritePlaces.getPlaceID());
        address.setText(favouritePlaces.getAddress());
        name.setTypeface(nicknormal);
        address.setTypeface(dense);

        return row;
    }
}


