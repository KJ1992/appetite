# README #

### What is this repository for? ###

This repository is JLPD Final Capstone application.
Here you will find 3 folders containing the website, android application and iOS application attempt.

### How do I get set up? ###

**Android - **
 For the android application, you will need android studio version 2.2.1 or above 
 along with SDK version Android 6.0 (Marshmellow) and API version 24 (usually installed with Marshmellow) or higher.
 

**Website - **
 For the website, you will need Visual Studio. No dependencies are required.
 

**iOS - **
 For the iOS, XCode is required to run the application.

### Who do I talk to? ###

Kurtis Johnson - Reposwitory Owner / Android Developer

Mharc Paccanas - Website Developer

Dhondup Dorjee - Website Developer

Solomon Laxmidhar - iOS Developer