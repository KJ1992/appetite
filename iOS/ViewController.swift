//
//  ViewController.swift
//  APPetite
//
//  Created by Solomon Laxmidhar on 2016-11-17.
//  Copyright © 2016 Solomon Laxmidhar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var homeMapView: MKMapView!
    @IBOutlet weak var campusPicker: UIPickerView!
    
    var campuses = ["Swipe up on me to pick your campus", "Trafalgar", "HMC", "Davis"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //load the campus picker
        self.campusPicker.dataSource = self
        self.campusPicker.delegate = self
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.campuses.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return campuses[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        NSLog("Row %ld, Text %@", row, campuses[row])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}